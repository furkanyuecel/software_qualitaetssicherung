package com.mycompany.app.my_app;

public interface ICalculator {
	
	public int addition(int num1, int num2);

    public int subtraction(int num1, int num2);

    public int multiplication(int num1, int num2);

    public int division(int num1, int num2);

}
